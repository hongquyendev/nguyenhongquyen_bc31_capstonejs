import { productsData } from "./productsData.js";

let arrPhone = [];

window.onload = () => {
  getApiPhone();
};

let getApiPhone = () => {
  productsData
    .getProductsData()
    .then((res) => {
      arrPhone = res.data;
      renderProductsData(arrPhone);
    })
    .catch((err) => {
      alert("khong lay duoc api");
    });
};

let renderProductsData = (array) => {
  let html = "";
  array.map((item, index) => {
    let { img, name, price, screen, backCamera, frontCamera, desc, type } =
      item;
    html += `
    
    
    <div class="col-3">
    <div class="card" style="width: 100%; height: 100%">
    <img class="card-img-top" src="${img}"/>
    <div class="card-body">
    <h5 class="card-title">${name}</h5>
    <p class="card-text">Giá: ${price}</p>
    <p class="card-text">Màn hình: ${screen}</p>
    <p class="card-text">Camera sau: ${backCamera}</p>
    <p class="card-text">  Camera trước: ${frontCamera}</p>
    <p class="card-text"> Mô tả: ${desc}</p>
    <p class="card-text"> Loại: ${type}</p>
    <button onclick="themVaoGioHang(${index})" class="btn btn-primary">Thêm vào giỏ</button>
    </div>
    </div>
    </div>
    
    
    `;
  });
  return (document.getElementById("body").innerHTML = html);
};

let themVaoGioHang = (spIndex) => {
  let index = tablePhone.findIndex((item) => {
    console.log("spIndex: ", spIndex);
    console.log("item.id: ", item.id);
    return item.id === spIndex;
  });

  if (index === -1) {
    console.log("index: ", index);
    let newPhone = { ...arrPhone[spIndex], soLuong: 1 };
    tablePhone.push(newPhone);
  } else {
    tablePhone[spIndex].soLuong++;
  }

  renderTable(tablePhone);
};

window.themVaoGioHang = themVaoGioHang;

let tablePhone = [];

let renderTable = (arr) => {
  let html = "";
  arr.map((item) => {
    let { id, img, price, name, desc, soLuong } = item;
    return (html += `
      <tr class="table">
      <td>${id}</td>
      <td>${name}</td>
      <td>${price}</td>
      <td>${desc}</td>
      <td><img src="${img}" style="width:40px"/></td>
      <td>${soLuong}</td>
      </tr>

      `);
  });
  return (document.getElementById("tablegiohang").innerHTML = html);
};
