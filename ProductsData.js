const CONST_URL = "https://62ebd2a955d2bd170e770cfb.mockapi.io/Products";
export const productsData = {
  getProductsData: () => {
    return axios({
      url: CONST_URL,
      method: "GET",
    });
  },
};
